/**
 * 
 */
package fr.ib.mickael.cuisine;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet MVC pour les équivalences entre masses
 * @author mic
 *	
 */
public class EquivalenceServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String[] UNIT_NAMES = {
			"grammes", "kilogrammes", "litres", "pincées", "cuillères à café", 
			"cuillères à soupe", "tasses"			
	};
	private static final Double[] UNIT_IN_GRAMMS = {
			1.0, 1000.0, 1000.0, 0.4, 1.0, 3.0, 50.0
	};
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setAttribute("quantity", req.getParameter("quantity"));
		req.setAttribute("source", req.getParameter("source"));
		req.setAttribute("unitNames", UNIT_NAMES);
		req.setAttribute("unitInGrams", UNIT_IN_GRAMMS);
		req.getRequestDispatcher("/WEB-INF/views/equivalence.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			double quantity = Double.parseDouble(req.getParameter("quantity"));
			int source = Integer.parseInt(req.getParameter("source"));
			ArrayList<String> results = new ArrayList<>();
			for(int i=0;i<UNIT_IN_GRAMMS.length;i++) {
				double result = UNIT_IN_GRAMMS[source] * quantity / 
						UNIT_IN_GRAMMS[i];
				results.add(Math.round(result)+" "+UNIT_NAMES[i]);
			}
			req.setAttribute("results", results); 
		} catch(Exception ex) {
			// une des valeurs ne permet pas de calcul
			req.setAttribute("message", "Erreur de calcul");
		}
		doGet(req, resp);
	}
	
}
