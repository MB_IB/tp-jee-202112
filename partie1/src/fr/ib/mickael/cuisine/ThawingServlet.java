/**
 * 
 */
package fr.ib.mickael.cuisine;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet MVC pour le calcul d'un temps de décongélation
 * @author mic
 *	
 */
public class ThawingServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final int MIN_BY_KG_AIR = 10*60;
	private static final int MIN_BY_KG_OVEN = 12;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/views/thawing.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		try {
			double mass = Double.parseDouble(req.getParameter("mass"));
			double resAir = mass*MIN_BY_KG_AIR;
			double resOven = mass*MIN_BY_KG_OVEN;
			req.setAttribute("message", "Décongélation : "+
					Math.round(resAir)+"min à l'air libre, "+
					Math.round(resOven)+"min au four");					
		} catch(Exception ex) {
			req.setAttribute("message", "Erreur de calcul");
		}
		doGet(req, resp);
	}
	
}
