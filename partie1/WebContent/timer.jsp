<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  %>
 <%-- Formulaire traité en Javascript --%>
<!DOCTYPE html>
<html>
<jsp:include page="WEB-INF/views/head.inc.jsp" />
<body>
	<div id="container">
		<jsp:include page="WEB-INF/views/menu.inc.jsp" />
		<h1>Chronomètre</h1>
		<p><label>Heures : <input id="hours" type="number" min="0" max="12" /></label></p>
		<p><label>Minutes : <input id="minutes" type="number" min="0" max="59" /></label></p>
		<p><label>Secondes : <input id="seconds" type="number" min="0" max="59" /></label></p>
		<p><input id="start" type="button" value="Démarrer" /></p>
		<p><input id="reset" type="button" value="RAZ" /></p>
	</div>
	<script>
	let containerDiv = document.getElementById("container");
	let hoursIn = document.getElementById("hours");
	let minutesIn = document.getElementById("minutes");
	let secondsIn = document.getElementById("seconds");
	let startBt = document.getElementById("start");
	let resetBt = document.getElementById("reset");
	let timer = null
	startBt.addEventListener("click", function(){
		startBt.setAttribute("disabled", "disabled")
		// nouveau chronomètre, appelle une fonction chaque seconde
		timer = setInterval(function(){
			h = parseInt(hoursIn.value) 
			m = parseInt(minutesIn.value)
			s = parseInt(secondsIn.value)
			s--
			if(s==m==h==0) {
				reset()
				console.log("Beep !")
				// clignoter : 20 événements en 1s
				for(let i=0;i<10;i++) {
					setTimeout(function(){
						containerDiv.style.backgroundColor="black"
					}, 100*i);
					setTimeout(function(){
						containerDiv.style.backgroundColor="white"
					}, 100*i+50);					
				}		
			} else {
				if(s<0) {				
					s+=60
					m--
					if(m<0) {
						m+=60
						h--
					}
				}
			}
			hoursIn.value = h
			minutesIn.value = m
			secondsIn.value = s
		}, 1000)
	})	
	function reset() {
		clearInterval(timer)
		hoursIn.value = 0
		minutesIn.value = 3
		secondsIn.value = 0
		startBt.removeAttribute("disabled")		
	}
	resetBt.addEventListener("click", reset)
	reset()
	</script>
</body>
</html>