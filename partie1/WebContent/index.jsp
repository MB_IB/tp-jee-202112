<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  %>
<!DOCTYPE html>
<html>
<jsp:include page="WEB-INF/views/head.inc.jsp" />
<body>
	<div>
		<jsp:include page="WEB-INF/views/menu.inc.jsp" />
		<h1>La bonne cuisine</h1>
		<p>
			<img src="img/favicon.png" alt="logo de cuillère" class="smallimage">
			<img src="img/favicon.png" alt="logo de cuillère" class="smallimage">
			<img src="img/favicon.png" alt="logo de cuillère" class="smallimage">
		</p>
		<p>Ce site est une démonstration, un TP de JEE.</p>
		<p>Il présente de nombreuses fonctionnalités utiles pour des cuisiniers.</p>
		<p>@IB, Mickael Blanchard, 2021</p>
	</div>		
</body>
</html>