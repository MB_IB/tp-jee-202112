<%@ page import="java.time.LocalDateTime"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- Formulaire traité en Java --%> 
<!DOCTYPE html>
<html>
<jsp:include page="head.inc.jsp" />
<body>
	<div>
		<jsp:include page="menu.inc.jsp" />
		<h1>Décongélation</h1>
		<form method="post">
			<p><label>Masse en kg : <input name="mass" type="number" value="1"></label></p>
			<p><input type="submit" ></p>		
			<p>${message}</p>
		</form>
	</div>
</body>
</html>