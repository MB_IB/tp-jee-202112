<%@ page import="java.time.LocalDateTime"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- Formulaire traité en Java --%> 
<!DOCTYPE html>
<html>
<jsp:include page="head.inc.jsp" />
<body>
	<div>
		<jsp:include page="menu.inc.jsp" />
		<h1>Equivalences</h1>
		<form method="post">
			<p><label>Quantité : <input name="quantity" type="number" 
				value="${quantity}"></label></p>
			<p><label>Unité source : <select name="source">
				<%-- on envoie dans le formulaire que l'index de l'unité, pas son nom --%>
				<c:forEach items="${unitNames}" var="name" varStatus="status">
				<option value="${status.index}"
					<c:if test="${status.index==source}">selected</c:if>
				>${name}</option>
				</c:forEach>					
			</select></label></p>			
			<p><input type="submit" ></p>		
			<p>${message}</p>
			<c:if test="${results!=null}">
				<ul>
				<c:forEach items="${results}" var="result">
					<li>${result}</li>
				</c:forEach>
			</c:if>
		</form>
	</div>
</body>
</html>