TP revue de code - la cuisine

Notions vues :

Java, Spring, JSP, JavaScript, Revue de code

Sujet :

Une application est déjà réalisée. Une fois lancée, la page http://localhost:8081/gastronomy.jsp permet de faire des recherches gastromiques.
Le code contient des erreurs trouvables par une revue de code soigneuse. Elles sont 5 dans chacune de ces 4 catégories :
- code illisible/inutile/simplifiable,
- respect de normes et homogénéité/normes implicites,
- documentation et log incorrects ou manquants,
- robustesse, bugs potentiels, données imprévues.
Ignorer les erreurs découvrables par des tests, ainsi que celles découvrables par une analyse statique quelconques (par exemple les avertissements dans Eclipse, ou son reformattage automatique) afin de ne rapporter que les défauts spécifiquement observables par une revue de code a l'oeil nu. N'observer que les 3 fichiers de code de cette itération (le package Java ....rc et la JSP associée)

Modalités :

Projet individuel, 1h30 de réalisation (pause éventuelle inclue) ; commencer par télécharger, inclure dans un projet précédent, et tester. Puis réaliser la revue de code.

Rendu :

Au bout de 90min exactement, poster un document HTML complet présentant cette revue de code (quelques informations utiles à trouver telle que la date de la relecture par exemple) et listant tous les défauts, chacun comportant ces 6 informations : un identifiant, l'emplacement exact du défaut, une catégorie, un titre rapide, une explication complète (une ou plusieurs phrases en bon français de 10 à 50 mots) et une proposition de solution partielle ou complète. Les défauts sont à ordonner logiquement.
