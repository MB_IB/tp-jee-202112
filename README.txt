Projet école 1 - la cuisine

Notions vues :

Java, HTML, CSS, JavaScript, Servlets, JSP

Sujet :

Une application est réalisée en JEE. Le code est propre et maintenable, les fichiers bien nommés.

Des cuisiniers nécessitent une application web interne. Celle-ci propose de multiples pages :
- une page d'accueil présente le site (HTML)
- une page permet de chronométrer (JavaScript) : choisir un temps en heurs, minutes et secondes, appuyer sur un bouton, le chronomètre démarre et la page clignote (voire émet un son) lorsque le temps est écoulé. Un bouton additionel permet de réinitialiser le formulaire
- une page permet de calculer une temps de décongélation (Servlet+JSP) : indiquer dans un formulaire une masse en kg, et un bouton d'envoi. Au retour du formulaire, afficher le temps calculé, à l'air libre et en four (respectivement 10h/kg et 12min/kg).
- une page indique les équivalences (Servlet+JSP) : choisir une quantité et une unité (parmis g, kg, litre, pincée, cuillère à café, cuillère à soupe, tasse...), et appuyer sur un bouton d'envoi pour afficher l'équivalent dans toutes les autres unités (pour de l'eau ou un ingrédient similaire)

Toutes les pages contiennent un menu identique pour se rendre vers les autres pages.
Le style graphique rapelle une, un livre de cuisine des années 1950/60 (CSS).

Modalités :

Projet en groupes aléatoires de 3 personnes, 13h de réalisation en 2 itérations de 7h et 6h ; sujet donné en début de première journée, annonce dans la première heure, de la part du groupe, de l'intention du rendu de la 1ère itération, rendu à la 7h de la 1ère itération et à la 13ème heure de la seconde

Groupes :
(Nouhaila), Theo, Gautier, Nicolas b, Nicolas 1j absent
Océane, Tristan, Dorra b, code redondants
Alexis, Océanne, Emile tb

Rendu :
- Au bout d'une heure : un cours document .txt (100 mots) annonçant le rendu de la 1ère itération
- Au bout de 7h : projet Web Eclipse (1ere itération) exporté dans un .zip
- Au bout de 13h : projet Web Eclipse (2eme itération) exporté dans un .zip

--------

Projet école 2 - la cuisine

Notions vues :

Java, HTML, CSS, Spring, JPA

Sujet :

Une application est réalisée en JEE avec Spring Boot et JPA/Hibernate. Le code est propre et maintenable, les fichiers bien nommés.

Une application de cuisine en ligne est complétée. Des cuisiniers nécessitent une page supplémentaire, accessible par un port IP différent (l'application fonctionne donc en parallèle avec un serveur Tomcat). Celle-ci présente en haut un formulaire de création de plat (nom, description, classification et prix, par exemple ["Omelette au fromage", "spécialité aux oeuf avec du Gruyère de Suisse", "plat principal", 8.90]), et en bas un tableau des plats déja créés, ordonnés par classification puis par nom. Eventuellement (optionnel), un lien sur chacun permet d'entrainer la suppression du plat.

Cette pages présente un style graphique identique aux précédente, et apparait dans le menu précédent.

Modalités :

Projet en groupes identique aux précédent, 6h de réalisation ; sujet donné précédement, rendu à la 6ème heure.

Groupes :
(Nouhaila, Theo,) Gautier, (Nicolas)
(Océane, )Tristan, Dorra : Discord
(Alexis, Océanne, Emile)

Rendu :
- Au bout de 6h : adresse (URL) d'un projet publique sur une forge logicielle

--------

Projet école 3 - la cuisine

Notions vues :

Java, HTML, CSS, Spring, REST, JPA/Hibernate

Sujet :

Une application est JEE avec Spring Boot et JPA/Hibernate est complété par un service web REST (plusieurs classes) et une JSP. Le code est propre et maintenable, les fichiers bien nommés.

Une application de cuisine en ligne est complétée. Des cuisiniers nécessitent une page supplémentaire, sur le port du TP 2, pour les réservations. Celle-ci présente en haut un formulaire de réservation : date&heure, nom, nombre de convives, en bas un tableau des réservations à venir, ordonnés par date&heure, actualisé à chaque ajout (sans recharger toute la page mais en profitant du service web REST). Un lien sur chacun permet d'entrainer la suppression de la réservation. Eventuellement, un autre lien permet de modifier cette réservation dans le formulaire au-dessus (fin du CRUD complet).

Cette page présente un style graphique identique aux précédentes, et apparait dans le menu précédent.

Modalités :

Projet en groupes identique aux précédent, 9h de réalisation ; sujet donné au début de la 1ere heure, rendu à la 9ème heure.

Groupes :

Nouhaila, Theo, Gautier, Nicolas
Océane, Tristan, Dorra
Alexis, Océanne, Emile

Rendu :
Au bout de 9h :
- adresse (URL) d'un projet publique sur une forge logicielle
- présentation orale de 20 minutes du projet - date, groupes, etc. puis chronologie du projet, détail de la solution du 3ème TP et ajouts possibles (fonctionnels ou autres), appuyée par une démonstration en direct et un Powerpoint de 5 à 10 pages.


(c) Mickael Blanchard, 2021-2022