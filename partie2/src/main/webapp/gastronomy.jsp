<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/views/head.inc.jsp" />
<body>
	<div>
		<jsp:include page="/WEB-INF/views/menu.inc.jsp" />
		<h1>Recherche des plats par lieu</h1>
		<p><label>Lieu : <input id="inLocation"></label></p>
		<p>Exemples : Toulouse, Pékin, Ardèche (département), Italie</p>
		<p><input type="button" value="Trouver" id="btFind"></p>
		<ul id="ulResult"></ul>
	</div>
	<script>
let inLocation = document.getElementById("inLocation")
let btFind = document.getElementById("btFind")
let ulResult = document.getElementById("ulResult")

/**
 * clic sur le bouton de recherche
 */
function btFindClick(){
	btFind.disabled = true
	let xhr = new XMLHttpRequest()
	xhr.open("GET", "http://localhost:8081/api/"+inLocation.value+"/dishes")
	xhr.onreadystatechange = findReadyStateChange
	xhr.send()
}

/**
 * Appelée pendant la requête/réponse AJAX
 */
function findReadyStateChange(){
	if(this.status == 200 && this.readyState==4) {
		console.log("Find response 200 / 4")
		if(this.responseText=="") {
			ulResult.innerHTML = "<li>Il n'y a pas de résultat</li>"
		} else {
			let dishes = JSON.parse(this.responseText)
			let html = ""
			for(let dish of dishes) {
				html += "<li>"+dish+"</li>"				
			}
			ulResult.innerHTML = html
		}
		btFind.disabled = false
	}	
	if(parseInt(this.status / 100) == 5) {
		ulResult.innerHTML = "<li>Erreur sur le serveur</li>"
		btFind.disabled = false
	}
}	

btFind.addEventListener("click", btFindClick)

	</script>
</body>
</html>
