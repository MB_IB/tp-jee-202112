<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/views/head.inc.jsp" />
<body>
	<div>
		<jsp:include page="/WEB-INF/views/menu.inc.jsp" />
		<h1>La bonne cuisine - nos plats</h1>
		<section>
			<h2>Nouveau plat</h2>
			<form action="/send" method="post">
			<p><label>Nom
				<input name="name" required />
			</label></p>
			<p><label>Informations
				<input name="comments" required />
			</label></p>
			<p><label>Catégorie
				<select name="category">
					<c:forEach items="${categories}" var="category">
					<option>${category}</option>
					</c:forEach>
				</select>
			</label></p>
			<p><label>Prix
				<input type="number" name="price" step="0.01" required /> €
			</label></p>
			<p>
				<input type="submit" value="Enregistrer" />
			</p>
			</form>
		</section>
		<section>
			<h2>Liste complète</h2>
			<table>
				<thead>
					<tr>
						<th>Catégorie</th>
						<th>Nom</th>
						<th>informations</th>
						<th>Prix</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${dishes}" var="d">
						<tr>
							<td>${d.category}</td>
							<td>${d.name}</td>
							<td>${d.comments}</td>
							<td><fmt:formatNumber value="${d.price}" 
								minFractionDigits="2" maxFractionDigits="2" /> €</td>
							<td><a href="/remove?id=${d.id}">Supprimer</a></td>
						</tr>
					</c:forEach>
				</tbody>						
			</table>
		</section>
	</div>		
</body>
</html>