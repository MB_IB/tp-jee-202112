<%-- Liens vers les deux projets --%>
<nav>
	<ul>
		<li><a href="http://localhost:8080/tp_cuisine/">Accueil</a></li>
		<li><a href="http://localhost:8080/tp_cuisine/timer.jsp">Chronomètre</a></li>
		<li><a href="http://localhost:8080/tp_cuisine/thawing">Décongélation</a></li>
		<li><a href="http://localhost:8080/tp_cuisine/equivalence">&Eacute;quivalences</a></li>
		<li><a href="http://localhost:8081/show">Plats</a></li>
		<li><a href="http://localhost:8081/gastronomy.jsp">Recherche</a></li>
	</ul>
</nav>	