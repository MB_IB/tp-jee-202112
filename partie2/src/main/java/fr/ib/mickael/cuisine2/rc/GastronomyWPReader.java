package fr.ib.mickael.cuisine2.rc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.json.JsonParser;
import org.springframework.stereotype.Service;

/**
 * Service utilitaire pour trouver le détail de plats d'un lieu dans Wikipedia
 * @author mic
 */
@Service
public class GastronomyWPReader {

	/**
	 * Titres de pararaphe possibles pour la section Wikipedia 
	 */
	private final String[] USEFUL_FOOD_TITLES = { "Spécialités culinaires", 
			"Cuisine", "Gastronomie" };
	/**
	 * Taille (en charactère) minimum pour prendre en compte une ligne 
	 */
	private final int MINIMUM_LINE_SIZE = 10;
	/**
	 * URL à appeler, voir https://www.mediawiki.org/w/api.php?action=help&modules=query%2Bextracts 
	 */
	private final String API_URL = "https://fr.wikipedia.org/w/api.php?action=query&format=json&"
			+ "prop=extracts&explaintext=true&titles=%s";

	/**
	 * Logger utile pour débug
	 */
	private final Logger logger = LoggerFactory.getLogger(GastronomyWPReader.class);

	/**
	 * Renvoie des plats pour un lieu
	 * 
	 * @param location lieu demandé
	 * @return liste de plats décrits
	 */
	public String[] getDishesForLocation(String location) {
		String locationPage = getLocationPage(location);
		return getDishesFromPage(locationPage);
	}

	/**
	 * Trouve les lignes de texte des chapitres de gastromie d'une page WP
	 * 
	 * @param page contenu de la page WP
	 * @return lignes de texte brut
	 */
	public String[] getDishesFromPage(String page) {
		for (String word : USEFUL_FOOD_TITLES) {
			Pattern paragraphRegexp = Pattern.compile("(?i)={2,4}\\s*" 
					+ word + "\\s*={2,4}([^=]+)=");
			Matcher paragraphMatcher = paragraphRegexp.matcher(page);
			// si ça correspond
			if (paragraphMatcher.find()) {
				// le groupe 1 est l'expression entre parenthèses si dessus
				String[] lines = paragraphMatcher.group(1).split("\n");
				ArrayList<String> usefulLines = new ArrayList<>();
				for (String line : lines) {
					if (line.trim().length() > MINIMUM_LINE_SIZE)
						usefulLines.add(line.trim());
				}
				return usefulLines.toArray(new String[] {});
			}
		}
		return null; // pas trouvé
	}

	/**
	 * renvoie une page Wikipedia pour un lieu
	 * 
	 * @param location lieu demandé
	 * @return contenu HTML de page Wikipedia en français
	 */
	@SuppressWarnings("unchecked")
	private String getLocationPage(String location) {
		String locationJsonData = getLocationJsonData(location);
		
		// Transformateur du texte JSOn en objets Java
		JsonParser jsonParser = new JacksonJsonParser();
		// La réponse JSON est toujours organisée : 
		// query > pages > (id) > extract > (contenu utile)  
		Map<String, Object> fullPage = jsonParser.parseMap(locationJsonData);
		Map<String, Object> query = (Map<String, Object>) fullPage.get("query");
		Map<String, Object> pages = (Map<String, Object>) query.get("pages");
		// première (seul) page, quelque soit son identifiant
		Map<String, Object> page = (Map<String, Object>) pages.values().toArray()[0];
		return (String) page.get("extract");
	}

	/**
	 * Renvoie le résultat d'un requête à l'API de wikipedia
	 * 
	 * @param location lieu demandé
	 * @return données JSON
	 */
	private String getLocationJsonData(String location) {
		try {
			String urlStr = String.format(API_URL, URLEncoder.encode(location, "UTF-8"));
			logger.debug("Will open " + urlStr);
			URL url = new URL(urlStr);
			URLConnection conn = url.openConnection();
			InputStream is = conn.getInputStream();
			
			// transformation du flux binaire en quelque chose de lisible
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String line, response = "";
			
			// lcture de toute la réponse
			while ((line = br.readLine()) != null)
				response += line;
			br.close();
			logger.debug("Received " + response.length() + " characters in JSON");
			
			return response;
		} catch (IOException ex) {
			logger.error("Error in response : ", ex);
			return null;
		}
	}
}
