package fr.ib.mickael.cuisine2.rc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Classe de controlleur pour le service web de gastronomie, utilisé seulement par
 * gastronomy.jsp
 * @author mic
 */
@RestController
public class GastronomyController {
	
	/**
	 * service de lecture Wikipedia
	 */
	private GastronomyWPReader gastronomyWPReader;
	
	/**
	 * Traitement d'une requête API : lire des plats
	 * @param location emplacement (ville, pays...)
	 * @return les lignes décrivant les plats trouvés en correspondance
	 */
	@RequestMapping(method = RequestMethod.GET, path="/api/{location}/dishes",
			produces="application/json")
	public String[] getDishes(@PathVariable("location") String location) {
		return gastronomyWPReader.getDishesForLocation(location);
	}
	
	
	/**
	 * Traitement d'une requête API : lire un des plats
	 * @param location emplacement (ville, pays...)
	 * @param num numéro (0...) du plat
	 * @return les lignes décrivant les plats trouvés en correspondance
	 */
	@RequestMapping(method = RequestMethod.GET, path="/api/{location}/dishes/{num}",
			produces="application/json")
	public String getDish(@PathVariable("location") String location,
			@PathVariable("location") int num) {
		return getDishes(location)[num];
	}

	@Autowired
	public void setGastronomyWPReader(GastronomyWPReader gastronomyWPReader) {
		this.gastronomyWPReader = gastronomyWPReader;
	}
}
