package fr.ib.mickael.cuisine2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * @author mic
 *
 */
@SpringBootApplication
public class Application {
	
	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {		
		SpringApplication.run(Application.class, args);
		logger.info("Application cuisine2 correctement démarrée");
	}

	// Spring Boot web utilise le "viewResolver" automatiquement
	@Bean(name="viewResolver")
	public ViewResolver getViewResolver() {
		// résolveur lisant à l'intérieur du JAR fabriqué
		InternalResourceViewResolver vr = new InternalResourceViewResolver();
		vr.setViewClass(JstlView.class); // type de vue : JSP+JSTL
		vr.setPrefix("/WEB-INF/views/");
		vr.setSuffix(".jsp");
		return vr;
	}
}
