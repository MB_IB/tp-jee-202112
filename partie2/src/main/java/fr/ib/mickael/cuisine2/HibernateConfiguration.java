package fr.ib.mickael.cuisine2;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * A faire en plus dans une base MySQL 5 (en tant que root) :
 * create database cuisine;
 * create user cuisinier@'%' identified by 'password';
 * grant all privileges on cuisine.* to cuisinier@'%';
 * 
 * @author mic
 */
@Configuration
public class HibernateConfiguration {
	
	@Bean
	public SessionFactory sessionFactory() {
		Properties options = new Properties();
		options.put("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
		options.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
		options.put("hibernate.connection.url", "jdbc:mysql://localhost/cuisine");
		options.put("hibernate.connection.username", "cuisinier");
		options.put("hibernate.connection.password", "password");
		options.put("hibernate.hbm2ddl.auto", "update"); 
//		options.put("hibernate.show_sql", "true");		
		SessionFactory factory = new org.hibernate.cfg.Configuration().
				addProperties(options).
				addAnnotatedClass(Dish.class).
				buildSessionFactory();				
		return factory;
	}
}
