package fr.ib.mickael.cuisine2;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author mic
 *
 */
@Controller
public class DishesController {
	private static final Logger logger = LoggerFactory.getLogger(DishesController.class);
	private static final String [] CATEGORIES = {
		"Hors d'oeuvre", "Entrée", "Plat principal", "Dessert"	
	};
	
	private SessionFactory sessionFactory;

	@RequestMapping("/show")
	public String showAll(Model model) {
		Session session = sessionFactory.openSession();
		List<Dish> dishes = session.createQuery(
				"from Dish order by category, name", Dish.class).list();
		model.addAttribute("dishes", dishes);
		model.addAttribute("categories", CATEGORIES);
		session.close();
		return "dishes";
	}
	
	@RequestMapping(path="/send", method=RequestMethod.POST)
	public String sendADish(@RequestParam String name, 
			@RequestParam String comments, 
			@RequestParam String category, 
			@RequestParam float price) {
		Dish d = new Dish(name, comments, category, price);
		logger.info("Received, save now : "+d);
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(d);
		tx.commit();
		session.close();
		// redirection HTTP 302 afin d'éviter un repost
		return "redirect:/show";
	}
	
	@RequestMapping(path="/remove")
	public String sendADish(@RequestParam int id) {
		logger.info("Delete dish id="+id);
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Dish d = session.get(Dish.class, id);
		session.delete(d);
		tx.commit();
		session.close();
		return "redirect:/show";
	}
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
