package fr.ib.mickael.cuisine2;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Un plat, tel qu'enregistré dans la base. Classe déclarée dans la
 * configuration d'Hibernate
 * 
 * @author mic
 */
@Entity
public class Dish {
	private int id;
	private String name;
	private String comments;
	private String category;
	private float price;

	public Dish() {
		this(null, null, null, 0);
	}

	public Dish(String n, String c, String cat, float p) {
		id = 0;
		name = n;
		comments = c;
		category = cat;
		price = p;
	}

	@Override
	public String toString() {
		return name + " (" + comments + ") - " + category + ", " + price + "e";
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false, length=100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(nullable = false, length=1000)
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Column(nullable = false, length=32)
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Column(nullable = false)
	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
